$(function ()
{

    var serviceUrl = "http://legal_info.ferrero.de/de/get_imprint";
    var aspToXMLFile = "asp/legalinfo.asp";

    // Testing
//    var aspToXMLFile = "http://www.kmpkt.com/clients/lared/ferrero/chocofresh/php/legalinfo.php";

//    function createCORSRequest(method, url)
//    {
//        var xhr = new XMLHttpRequest();
//        if ("withCredentials" in xhr)
//        {
//
//            // Check if the XMLHttpRequest object has a "withCredentials" property.
//            // "withCredentials" only exists on XMLHTTPRequest2 objects.
//            xhr.open(method, url, true);
//
//        } else if (typeof XDomainRequest !== "undefined")
//        {
//            // Otherwise, check if XDomainRequest.
//            // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
//            xhr = new XDomainRequest();
//            xhr.open(method, url);
//
//        } else
//        {
//            // Otherwise, CORS is not supported by the browser.
//            console.log('CORS is not supported');
//            xhr = null;
//        }
//        return xhr;
//    }

//    var xhr = createCORSRequest('GET', url);
//    if (!xhr)
//    {
//        throw new Error('CORS not supported');
//    }

//    xhr.onload = function ()
//    {

    $.post(aspToXMLFile, { url: serviceUrl }, function(data)
    {
        var xml = data;
        var jsonObj = $.xml2json(xml);

        var directive = {
//            'title': 'imprint.title',
            'name': 'name',
            'street': 'street',
            'location': 'location',
            'telefon': 'telefon',
            'fax': 'fax',
            'managmentTitle': 'management.title',
            'managmentContent': 'management',
            'traderegisterTitle': 'traderegister.title',
            'traderegisterContent': 'traderegister',
            'taxidTitle': 'taxid.title',
            'taxidContent': 'taxid'
        };


        var directiveFnc = function (node, nodeName)
        {
            var multiTemplate = $p('item').compile(multiParagraphFnc(nodeName));
            var singleTemplate = $p('item').compile(singleParagraphFnc(nodeName));
            var directive = {
                '.': function ()
                {
                    return (node.paragraph instanceof Array) ? multiTemplate(this) : singleTemplate(this);
                }
            };
            return directive;
        };

        var multiParagraphFnc = function (node)
        {
            var multiParagraph = {'h2': (node + '.title')};
            var p = {};
            p['subitem <- ' + node + '.paragraph'] = {'.': 'subitem'};
            multiParagraph['p'] = p;
            return multiParagraph;
        };

        var singleParagraphFnc = function (node)
        {
            return {
                'h2': (node + '.title'),
                'p': (node + '.paragraph')
            };
        };
        $('div.copyright').render(jsonObj, directiveFnc(jsonObj.copyright, 'copyright'));
        $('div.disclaimer').render(jsonObj, directiveFnc(jsonObj.disclaimer, 'disclaimer'));
        $('div#email a').autoRender(jsonObj);
        $('div.template').render(jsonObj, directive);
        $('div.introduction').autoRender({'introduction': $(xml).find('introduction').text()});
//        $('h1.title').autoRender({'title': $(xml).filter(":first").attr("title")});

        $('div#email a').attr('href', 'mailto:' + $('div#email a').attr('href'));

        $('#container').show();
    });


});