$(function ()
{

    var serviceUrl = "http://legal_info.ferrero.de/de/get_dps_instance?registration=0&usercontributedcontent=0&competition=0&games=0&newsletter=0&getadvertisement=0&orders=0&socialnetwork=0&showadvertisment=0&googleanalytics=1";
    var aspToXMLFile = "asp/legalinfo.asp";

    // Testing
//    var aspToXMLFile = "http://www.kmpkt.com/clients/lared/ferrero/chocofresh/php/legalinfo.php";
//    var aspToXMLFile = "http://kinderchocofresh-de.inf.stormreply.net/asp/legalinfo.asp";

    $.post(aspToXMLFile, { url: serviceUrl }, function (data)
    {
        var xml = data;
        var jsonObj = $.xml2json(xml);

        var directive = {
            'chapter': {
                'chapt <- chapter': {
                    '.': function ()
                    {
//                        console.log(this);
                        return (this.paragraph instanceof Array) ? multiTemplate(this) : singleTemplate(this);
                    }
                }
            }
        };

        var multiParagraph = {
            'h2': 'title',
            'p': {
                'subitem <- paragraph': {
                    '.': 'subitem'
                }
            }
        };

        var singleParagraph = {
            'h2': 'title',
            'p': 'paragraph'
        };

        var multiTemplate = $p('item').compile(multiParagraph);
        var singleTemplate = $p('item').compile(singleParagraph);

        $('div.template').directives(directive).render(jsonObj).appendTo('.result');
//        $('h1.title').autoRender({'title': $(xml).filter(":first").attr("title")});


        $('p').each(function ()
        {
            var p = $(this);
            p.html(p.text().replace('[[SITE_URL][SITE_URL_BRIEF]]', '<a href="http://www.chocofresh.de" target="_blank">www.chocofresh.de</a>'));
        });

        /**
         * Search for placeholder: e.g. [[mailto:consumerservice@ferrero.com][consumer-service]] and replace it with <a> tag
         */

        $('p').each(function ()
        {
            var p = $(this);
            var regExp = /\[\[(.+?)\]\]/g;
            var matches = p.text().match(regExp);

            if (matches)
            {
                var btnStr;
                for (var i = 0; i < matches.length; i++)
                {
                    var firstItem = matches[i].split("][");
                    var mailToStr = firstItem[0].substr(2);

                    if (mailToStr.search(/^http[s]?:\/\//))
                    {
                        btnStr = mailToStr.split(":")[1];

                    } else
                    {
                        btnStr = mailToStr;
                    }
                    p.html(p.text().replace(/\[\[(.+?)\]\]/, "<a href='" + mailToStr + "'>" + btnStr + "</a>"));
                }
            }
        });
        $('#container').show();
    });


});