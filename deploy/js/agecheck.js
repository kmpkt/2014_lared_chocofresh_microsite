/*jshint devel:true */
/*global dataLayer */

var inputIdList = ['day1', 'day2', 'month1', 'month2', 'year1', 'year2', 'year3', 'year4'];
var inputIndex = 1;
//
var submitButton = document.getElementById('submit-btn');
var scope = this;

// For IE8 and earlier version.
if (!Date.now)
{
    Date.now = function ()
    {
        return new Date().valueOf();
    };
}


function getAge(birthDate)
{
    var ageDifMs = Date.now() - birthDate;
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    var res = (ageDifMs < 0) ? NaN : Math.abs(ageDate.getUTCFullYear() - 1970);
    return res;
}


submitButton.onclick = function ()
{
    var ageCheckBox = document.getElementById('agecheck');
    var accessDeniedBox = document.getElementById('access-denied');
    var lightBox = document.getElementById('lightbox');
    var modalLayer = document.getElementById('modal');
    var errorMsg = document.getElementById('error-msg');

    var day1 = document.getElementById(inputIdList[0]).value;
    var day2 = document.getElementById(inputIdList[1]).value;
    var month1 = document.getElementById(inputIdList[2]).value;
    var month2 = document.getElementById(inputIdList[3]).value;
    var year1 = document.getElementById(inputIdList[4]).value;
    var year2 = document.getElementById(inputIdList[5]).value;
    var year3 = document.getElementById(inputIdList[6]).value;
    var year4 = document.getElementById(inputIdList[7]).value;
    var age;

    if (year1 && year2 && year3 && year4)
    {
        var birthDate = Date.UTC(year1 + year2 + year3 + year4, month1 + month2, day1 + day2);
        birthDate = birthDate - 2629800000;

        if ((month1 < 1 && month2 < 1) || (day1 < 1 && day2 < 1) || (Number(month1 + month2) > 12) || (Number(day1 + day2) > 31))
        {
            age = NaN;
        }
        else
        {
            age = (birthDate < -2239056000000 ) ? NaN : getAge(birthDate);
        }


        if (age < 12 && age >= 0)
        {
            ageCheckBox.style.display = 'none';
            accessDeniedBox.style.display = 'block';
        } else if (age >= 12)
        {
            // Trigger tracking, when age check succeeded.
            dataLayer.push({'event':'age','ageformstatus': '0'});
            lightBox.style.display = 'none';
            modalLayer.style.display = 'none';
            var flashObj = document.getElementById('chocofresh');
            flashObj.receiveJSData("startVideo");
        } else
        {
            errorMsg.style.display = 'block';
        }
    } else
    {
        errorMsg.style.display = 'block';
    }
};


function keyUpFnc(e)
{
    e = e || window.event;
    var target = e.target || e.srcElement;
    if (scope.inputIndex < scope.inputIdList.length)
    {
        scope.inputIndex = target.getAttribute('data-id');
        var el = document.getElementById(inputIdList[scope.inputIndex]);
        scope.inputIndex++;
        el.focus();
    }
}


function onFocusFnc (){
    this.value = '';
}


function init()
{
    var zIndexNumber = 1000;
    $('div').each(function ()
    {
        $(this).css('zIndex', zIndexNumber);
        zIndexNumber -= 10;
    });

    var container = document.getElementById('input-container');
    container.onkeyup = keyUpFnc;

    var inputs = document.getElementsByTagName('input');
    for (var i = 0; i < inputs.length; i++)
    {
        inputs[i].onfocus = onFocusFnc;
    }
}


function startVideoWhenRdy()
{
    var checkFlashStage = window.setInterval(function ()
    {
        if ((typeof document.getElementById('chocofresh')) !== 'undefined')
        {
            document.getElementById('chocofresh').receiveJSData('startVideo');
            window.clearInterval(checkFlashStage);
        }
    }, 500);
}


// Read a page's GET URL variables and return them as an associative array.
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


function checkUrlForAgeChecked()
{
    if (getUrlVars().age === 'notrequired')
    {
        startVideoWhenRdy();
    } else
    {
        var lightBox = document.getElementById('lightbox'),
            modalLayer = document.getElementById('modal');
        lightBox.style.display = 'block';
        modalLayer.style.display = 'block';
        // Trigger tracking, when age check is shown
        dataLayer.push({'event':'ageform','ageformstatus': '1'});
        init();
    }
}


/**
 * For example, if you have the URL:
 * http://www.example.com/?me=myValue&name2=SomeOtherValue
 * This code will return:
 *
 * {
 *  "me"    : "myValue",
 *  "name2" : "SomeOtherValue"
 *  }
 *
 * and you can do:
 *
 * var me = getUrlVars()["me"];
 * var name2 = getUrlVars()["name2"];
 *
 **/


$(function ()
{
    checkUrlForAgeChecked();
});