﻿package klassen 
{
	// Klassen importieren
	import flash.display.Sprite;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.events.Event;
	import flash.events.TextEvent;
	import flash.text.TextFormat;
	import flash.text.Font;
	import flash.text.AntiAliasType;
 
	public class TextHTML extends Sprite {
		// Textfeld anlegen
		private var _textinhalt:String = "";
		private var _txt:TextField = new TextField();
		private var _txtformat:TextFormat = new TextFormat();
		private var _myFontR:Font;
		
		// Konstruktor definieren
		public function TextHTML(content:String, breite:uint = 0) 
		{
			_textinhalt = content;
			
			// CSS laden
			var loader:URLLoader = new URLLoader();
			var req:URLRequest = new URLRequest("css/main.css");
			loader.addEventListener(Event.COMPLETE, cssLoaded);
			loader.load(req);
 
			// Textfeld formatieren und anzeigen
			_txt.multiline = true;
			_txt.wordWrap = true;
			_txt.antiAliasType = AntiAliasType.ADVANCED;
			_txt.width = breite;
			_txt.autoSize = TextFieldAutoSize.LEFT;
		}
 
		// Event-Handler für COMPLETE (CSS ist geladen)
		public function cssLoaded(event:Event):void {
			// Stylesheet zuweisen

			var css:StyleSheet = new StyleSheet();
			css.parseCSS(event.target.data);
			_txt.styleSheet = css;
 
			// HTML zuweisen
			
			//<p>
			//<a>
			//<br />
			
 
			// mit Hyperlink
			 //_txt.htmlText = "<a href='http://www.google.de'>begrüße</a>";		
			
			// mit internem Link
			_txt.htmlText = _textinhalt;
			addChild(_txt);
			//_txt.htmlText = "";
			_txt.addEventListener(TextEvent.LINK, onLinkClick);
		}
 
		// Event-Handler für LINK
		private function onLinkClick(event:TextEvent):void{
			trace(event.text);
		}
		
		public function GetHoehe():Number
		{
			return _txt.height;
		}
	}
}