﻿package klassen
{
	import com.adobe.images.BitString;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.system.LoaderContext;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.Security;
	import flash.events.SecurityErrorEvent;


	public class BildLoader extends MovieClip
	{
		public var myURL:String;
		public var bitmap:Bitmap;
		public var loaded:Boolean = false;
		var myLoader:Loader;
		//
		public function BildLoader(locate:String = null)
		{
			if (locate != null)
			{
				myURL = locate;
				loadImage();
			}
		}
		public function loadImage():void
		{
			if (myURL != null)
			{				
				var context:LoaderContext = new LoaderContext();
				context.checkPolicyFile = true;
				Security.loadPolicyFile("http://ansicht.chocofresh.de/crossdomain.xml");
				
				myLoader = new Loader();
				myLoader.contentLoaderInfo.addEventListener(Event.INIT, onComplete);
				myLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
				myLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
				var myRequest:URLRequest = new URLRequest(myURL);
				myLoader.load(myRequest, context);
			}
		}
		private function onComplete(event:Event):void
		{
			addChild(myLoader);
			loaded = true;
			dispatch();
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		private function dispatch():void
		{
			var event : HeigthEvent;    
			var type : String;
			type = HeigthEvent.COMPLETED;
			event = new HeigthEvent ( type );   // Porduzieren eines neuen Events mit meinem CountdownEvent-Klasse
			event.hoehe = -- myLoader.height;  // Event wird zusätzlich mitgeteilt, wieviel Restzeit noch übrig geblieben ist. Wird in die Variable von event geschrieben.
			dispatchEvent( event );
			//trace("Bild: "+event.hoehe);
		}
		private function onSecurityError(event:SecurityErrorEvent):void 
		{
			trace(event.text);
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		private function onIoError(event:IOErrorEvent):void
		{
			//trace("onIoError: " + event);
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public function get breite():Number
		{
			return myLoader.width;
		}
	}
}