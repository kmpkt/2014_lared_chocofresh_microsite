﻿package klassen 
{
	import flash.display.Sprite;
	import fl.controls.CheckBox; 
	import flash.events.MouseEvent;

	public class MyCheckbox extends Sprite
	{
		private var cb1:CheckBox;
		private var sel:Boolean = false;
		private var border:Sprite = new Sprite();
		
		public function MyCheckbox() 
		{
            cb1 = new CheckBox();
            cb1.label = "";
            cb1.y = 10;
            cb1.addEventListener(MouseEvent.CLICK,updateCart);
            addChild(cb1);
			ClearError();
        }

		
		private function updateCart(e:MouseEvent):void {
			//trace("You have selected:\n");
            if (cb1.selected == true) sel=true;
			if(cb1.selected == false) sel=false;
        }
		
		public function get selected_checkbox():Boolean
		{
			return sel;
		}
		
		public function SetError():void
		{
			removeChild(cb1);
			border.graphics.beginFill(0xCB0101);
			border.graphics.drawRect(5, 14, 14, 14);
			border.graphics.endFill();
			addChild(border);
			addChild(cb1);
			trace("SETError");
		}
		
		public function ClearError():void
		{
			removeChild(cb1);
			border.graphics.clear();
			border.graphics.beginFill(0xCBECFF);
			border.graphics.drawRect(5, 14, 14, 14);
			border.graphics.endFill();
			addChild(border);
			addChild(cb1);
			//trace("ClearError");
		}
		
	}

}