package klassen 
{
	import flash.events.Event;
 
	public class EingabeEvent extends Event   //CountdownEvent-Klasse soll später mein Ereignis sein oder besser eine Instance der CountdownEvent-Klasse 
	{
		public static const ERROR : String = "error";   // deklarieren von zwei Event-Typen
		public static const VALIDE : String = "valide";

	public function EingabeEvent ( type : String, bubbles : Boolean = false, cancelable : Boolean = false ) 
		{
			super ( type, bubbles, cancelable );
		}
	}
}