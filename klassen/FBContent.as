﻿package klassen 
{
	import adobe.utils.CustomActions;
	
	import com.adobe.serialization.json.JSON;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.system.Security;
	
	import klassen.BildLoader;
	import klassen.FBContent;
	import klassen.FBOjb;
	import klassen.TextHTML;
	
	public class FBContent extends Sprite
	{
		private var loader:URLLoader = new URLLoader();
		private var text:TextHTML;
		private var datum:TextHTML;
		private var bild:BildLoader;
		private var bg:Sprite = new Sprite;
		
		private var ObjArray:Array = new Array();;
		private var obj:Object;
		private var obj_source:Object;
		
		private var breite:uint =  250;
		private var objekt24:FBOjb;
		
		private var z:uint = 0;
		private var y1:int = 0;
		
		private var id_name:String = "chocofresh";
		
		
		public function FBContent()
		{
			var request:URLRequest = new URLRequest("http://www.chocofresh.de/services/FBFeedHandler.ashx");
			request.method = URLRequestMethod.POST;
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, completeHandler);
			
			try
			{
				loader.load(request);
			}
			catch (error:Error)
			{
				trace("Error loading requested document: ");
			}
		}
		
		private function completeHandler(event:Event):void
		{
			loader.removeEventListener(Event.COMPLETE, completeHandler);
			
			var obj3:String;
			obj3 = event.target.data;
			obj_source = JSON.decode(obj3);
			//trace("obj_source"+obj_source);
			obj = obj_source[id_name].posts;
			for (var i:int = 0; i < obj.data.length; i++) 
			{
				
				var formName:String = "";

				var formID:String = "";

				var message:String = "";

				var picture:String = "";

				var link:String = "";

				var source:String = "";

				var name:String = "";

				var caption:String = "";

				var description:String = "";

				var icon:String = "";

				var attribution:String = "";

				var type:String = "";

				var created_time:String = "";

				var updated_time:String = "";

				var likes:String = "";

				var commernts_count:String = "";


				if(obj.data[i].from)
				{
					if (obj.data[i].from.name)
					{
						formName = obj.data[i].from.name;
					}
					if (obj.data[i].from.id)
					{
						formID= obj.data[i].from.id;
					}
				}
				if(obj.data[i].message)
				{
					message = obj.data[i].message;
				}
				if(obj.data[i].picture)
				{
					picture = obj.data[i].picture;
				}
				if(obj.data[i].link)
				{
					link = obj.data[i].link;
				}
				if(obj.data[i].source)
				{
					//source = obj.data[i].source;
				}
				if(obj.data[i].name)
				{
					//name = obj.data[i].name;
				}
				if(obj.data[i].caption)
				{
					//caption = obj.data[i].caption;
				}
				if(obj.data[i].description)
				{
					//description = obj.data[i].description;
				}
				if(obj.data[i].icon)
				{
					//icon = obj.data[i].icon;
				}
				if(obj.data[i].attribution)
				{
					//attribution = obj.data[i].attribution;
				}
				if(obj.data[i].type)
				{
					type = obj.data[i].type;
				}
				if(obj.data[i].created_time)
				{
					//created_time = obj.data[i].created_time;
				}
				if(obj.data[i].updated_time)
				{
					updated_time = obj.data[i].updated_time;
				}
				if(obj.data[i].likes)
				{
					//likes = obj.data[i].likes;
				}
				if(obj.data[i].comments)
				{
					if(obj.data[i].comments.count)
					{
						//commernts_count = obj.data[i].comments.count;
					}
				}

				ObjArray[i] = [formName]; //0
				
				ObjArray[i].push(formID); //1
					
				ObjArray[i].push(message); //2
				
				ObjArray[i].push(picture); //3
				
				ObjArray[i].push(link); //4
			
				ObjArray[i].push(source); //5
			
				ObjArray[i].push(name); //6
			
				ObjArray[i].push(caption); //7
				
				ObjArray[i].push(description); //8
			
				ObjArray[i].push(icon); //9
				
				ObjArray[i].push(attribution); //10
				
				ObjArray[i].push(type); //11
				
				ObjArray[i].push(created_time); //12
				
				ObjArray[i].push(updated_time); //13
				
				ObjArray[i].push(likes);  //14
				
				ObjArray[i].push(commernts_count); //15
				
			}
			this.showNext();
		}
		
		private function showNext():void
		{
			if(z < ObjArray.length)
			{
				try
				{
					var obj:FBOjb = new FBOjb(ObjArray[z][0], ObjArray[z][1], ObjArray[z][2], ObjArray[z][3], ObjArray[z][4], ObjArray[z][5], 
						ObjArray[z][6], ObjArray[z][7], ObjArray[z][8], ObjArray[z][9],ObjArray[z][10], ObjArray[z][11], ObjArray[z][12], 
						ObjArray[z][13], ObjArray[z][14], ObjArray[z][15] );
					obj.y = y1;
					obj.addEventListener(Event.COMPLETE, fillObjekt);
					obj.addEventListener(Event.COMPLETE, drawBG);
				}
				catch(e:Error)
				{
					dispatchEvent(new Event(Event.COMPLETE));
				}
			}
			else
			{
				dispatchEvent(new Event(Event.COMPLETE));
			}
		}
		private function fillObjekt(e:Event):void
		{
			var obj:FBOjb = e.currentTarget as FBOjb;
			obj.graphics.beginFill(0xE9E9E9);
			obj.graphics.drawRect(0, obj.height+8, breite, 0.8);
			obj.graphics.endFill();
			if(obj.height > 20){addChild(obj);}
				y1 = Math.floor(y1 + obj.height);
			z++;
			this.showNext();		
		}
		
		
		private function drawBG(e:Event):void
		{
			bg.graphics.beginFill(0xffffff);
			bg.graphics.drawRect(0, 0, breite, e.target.y + e.target.height );
			bg.graphics.endFill();
			if(e.target.height > 20){addChildAt(bg, 0);}
		}
	}
}