package klassen 
{
	import fl.controls.ComboBox;
	import flash.display.Sprite;
	import flash.text.TextFormat;
	import flash.text.Font; 
	import fl.managers.StyleManager;
	

	public class DropDownMenu extends Sprite
	{
		private var _myFontR:Font;
		
		public function DropDownMenu() 
		{
			var txtformat:TextFormat = new TextFormat();
			var styleObj:Object;
			_myFontR = new FB_Feed();
			txtformat.font = _myFontR.fontName;
			txtformat.letterSpacing = 0.6;
			txtformat.size = 12;
			txtformat.color = 0x666666;
			
			var myComboBox:ComboBox = new ComboBox();
			
			myComboBox.addItem( { label:"Frau" } );
			myComboBox.addItem( { label:"Herr" } );
			myComboBox.move(10, 30);
			myComboBox.dropdownWidth = 103;
			StyleManager.setStyle("embedFonts", true);
			StyleManager.setStyle("textFormat", txtformat);

			addChild(myComboBox);

			var maxLength:Number = 0;
			var i:uint;
			for (i = 0; i < myComboBox.length; i++) {
				myComboBox.selectedIndex = i;
				myComboBox.drawNow();
				var currText:String = myComboBox.text;
				var currWidth:Number = myComboBox.textField.textWidth;
				maxLength = Math.max(currWidth, maxLength);
				trace(currText, currWidth, maxLength);
			}

			myComboBox.selectedIndex = -1;
		}
		
	}

}




