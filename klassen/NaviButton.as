package klassen 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	/**
	 * ...
	 * @author 
	 */
	public class NaviButton extends Sprite
	{
		private var neutral:Sprite;
		private var over:Sprite;
		public var activVar:Boolean = false;
		private var setnavVar:Boolean = false;
		
		public function NaviButton(p_nuetral:Sprite, p_over:Sprite) 
		{
			neutral = p_nuetral;
			over = p_over;
			
			//over.visible = false;
			
			
			addChild(neutral);
			addChild(over);
			neutral.buttonMode = true;
			over.buttonMode = true;
			over.alpha = 0;
		}
		
		private function mouse_over(e:MouseEvent):void
		{
			if (!activVar)
			{
				TweenLite.to(over, 0.4, { alpha:1, ease:Quad.easeIn, overwrite:1 } );
			}
		}
		
		private function mouse_out(e:MouseEvent):void
		{
			if (!activVar)
			{
				TweenLite.to(over, 0.4, { delay:0.25, alpha:0, ease:Quad.easeIn, overwrite:1 } );
			}
		}
		
		public function set activ(p_activ:Boolean):void
		{
			activVar = p_activ;
			if (!activVar)
			{
				TweenLite.to(over, 0.4, { alpha:0, ease:Quad.easeIn, overwrite:1 } );
			}
			else
			{
				TweenLite.to(over, 0.4, { alpha:1, ease:Quad.easeIn, overwrite:1 } );
			}
		}
		
		public function get activ():Boolean
		{
			return activVar;
		}
		
		public function setNavi(p_setnavVar:Boolean=false):void
		{
			setnavVar = p_setnavVar;
			if (setnavVar)
			{
				addEventListener(MouseEvent.MOUSE_OVER, mouse_over);
				addEventListener(MouseEvent.ROLL_OVER, mouse_over);
				addEventListener(MouseEvent.MOUSE_OUT, mouse_out);
			}
		}
		

	}

}