﻿package klassen 
{
	import klassen.E_AntwortPorxy;
	import flash.display.Sprite;
	import flash.net.navigateToURL;
	import flash.net.URLLoader;
	import flash.net.URLVariables;
	import flash.net.URLRequestMethod;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.events.*;
	import flash.events.EventDispatcher;

	public class AntwortProxy extends Sprite
	{
		
		private var loadURL:String = "/swf/VideoStop/";

		private var loader:URLLoader = new URLLoader();
		private var _dbAbfrage:String;
		
		private var r_antwort:uint;

		
		public function AntwortProxy(qString:String):void

		{
			var request1:URLRequest = new URLRequest(qString);
			request1.method = URLRequestMethod.POST;
			
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, completeHandler);
			
			try
			{
				loader.load(request1);
			}
			catch (error:Error)
			{
				trace("Error loading requested document: ");
			}
		}
		
		private function completeHandler(event:Event):void
		{
			trace("Complete");
			loader.removeEventListener(Event.COMPLETE, completeHandler);
			r_antwort = loader.data;
			var event1:E_AntwortPorxy = new E_AntwortPorxy ( E_AntwortPorxy.COMPLETE );
			event1.antwort = r_antwort;
			this.dispatchEvent( event1 );

		}
		
		
		private function httpStatusHandler(event:Event):void
		{
			trace("httpStatusHandler: " + event);
		}

		private function ioErrorHandler(event:IOErrorEvent):void
		{
			trace("ioErrorHandler: " + event);
		}

		private function openHandler(event:Event):void
		{
			trace("openHandler: " + event);
		}

		private function progressHandler(event:ProgressEvent):void
		{
			trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
		}

		private function securityErrorHandler(event:SecurityErrorEvent):void
		{
			trace("securityErrorHandler: " + event);
		}
	}
}