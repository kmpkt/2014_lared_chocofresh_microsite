package klassen 
{
	import flash.events.Event;
	
	public class E_AntwortPorxy extends Event
	{
		public static const COMPLETE:String = "complete";
		public var antwort:uint;
		
		public function E_AntwortPorxy(type:String, bubbles:Boolean = false, cancelable:Boolean = false) 
		{
			super(type, bubbles, cancelable);
		}
		
	}

}