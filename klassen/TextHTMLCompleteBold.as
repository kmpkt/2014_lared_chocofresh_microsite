﻿package klassen 
{
	// Klassen importieren
	import flash.display.Sprite;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.events.Event;
	import flash.events.TextEvent;
	import flash.text.TextFormat;
	import flash.text.Font;
	import flash.text.AntiAliasType;
	import flash.display.MovieClip;
	import flash.utils.setTimeout;
 
	public class TextHTMLCompleteBold extends Sprite {
		// Textfeld anlegen
		private var _textinhalt:String = "";
		private var _txt:TextField = new TextField();
		private var _txtformat:TextFormat = new TextFormat();
		
		//private var s:Schrift;
		private var _myFontR:Font;
		private var css_str:String = ".name {color:#F14A2A} .datum {color:#7C7C7C} h1 {font-size: 16px;leading: 10;} p {font-size: 12px; margin-bottom: 5px} .abstand {leading: 10;} a:hover, a:active {color:#5AC7FE; text-decoration:underline} .bold {font-weight:bold} .linkstyle { color:#5AC7FE }";

		/*[Embed(source='/Schriften.swc', symbol='FrutigerLT57')]
		private var FrutigerLT57 : Class;*/
		/*[Embed(source='Schriften.swc', symbol='FrutigerLT65')]
		var FrutigerLT57 : Class;
		[Embed(source='Schriften.swc', symbol='cooperStd')]
		var cooperStd : Class;*/
		
		public function TextHTMLCompleteBold(content:String, breite:uint = 0) 
		{
			_textinhalt = content;
			
			// CSS laden
			/*var loader:URLLoader = new URLLoader();
			var req:URLRequest = new URLRequest("css/main.css");
			//loader.addEventListener(Event.COMPLETE, cssLoaded);
			loader.load(req);*/
 
			// Textfeld formatieren und anzeigen
			_txt.multiline = true;
			_txt.wordWrap = true;
			_txt.antiAliasType = AntiAliasType.ADVANCED;
			_txt.width = breite;
			_txt.autoSize = TextFieldAutoSize.LEFT;
			_txt.backgroundColor = 0x000000;
			
			// TEXT-Format
			
			/*_myFontR = new MyFrutigerBold();
			//_txtformat.size = 13;
			_txtformat.font = _myFontR.fontName;
			*/
			
			_myFontR = new FB_Feed_Bold();
			_txtformat.size = 11;
			//_txtformat.letterSpacing = -1;

			_txtformat.bold = true;
			_txtformat.font = _myFontR.fontName;
			_txtformat.letterSpacing = 0;
			
			_txt.embedFonts = true;
			_txt.selectable = false;
			_txt.mouseWheelEnabled = false;
			_txt.antiAliasType = AntiAliasType.ADVANCED;
			_txt.defaultTextFormat = _txtformat;


			/*graphics.beginFill(0x111111);
			graphics.drawRect(0, 0, _txt.width, _txt.height);
			graphics.endFill();*/
			setTimeout(cssLoaded, 1);
		}
		
 
		// Event-Handler für COMPLETE (CSS ist geladen)
		public function cssLoaded():void {
			// Stylesheet zuweisen

			var css:StyleSheet = new StyleSheet();
			css.parseCSS(css_str);
			_txt.styleSheet = css;
 
			// HTML zuweisen
			
			//<p>
			//<a>
			//<br />
			
 
			// mit Hyperlink
			 //_txt.htmlText = "<a href='http://www.google.de'>begrüße</a>";		
			
			// mit internem Link
			_txt.htmlText = _textinhalt;
			//trace(_txt.height);
			addChild(_txt);
			
			
			dispatch();
			//_txt.htmlText = "";
			_txt.addEventListener(TextEvent.LINK, onLinkClick);
		}
		
		private function dispatch():void
		{
			var event : HeigthEvent;    
			var type : String;
			type = HeigthEvent.COMPLETED;
			event = new HeigthEvent ( type );   // Porduzieren eines neuen Events mit meinem CountdownEvent-Klasse
			event.hoehe = -- _txt.height;  // Event wird zusätzlich mitgeteilt, wieviel Restzeit noch übrig geblieben ist. Wird in die Variable von event geschrieben.
			dispatchEvent( event );
			//trace("Text: "+event.hoehe);
		}
 
		// Event-Handler für LINK
		private function onLinkClick(event:TextEvent):void{
			//trace(event.text);
		}
		
		public function GetHoehe():Number
		{
			return _txt.height;
		}
	}
}