﻿package klassen
{
	import flash.display.Sprite;
	import flash.net.URLRequest; 
	import flash.display.Loader; 
	import flash.events.Event; 
	import flash.events.ProgressEvent; 
	import flash.display.MovieClip;

	public class SWFLoader extends MovieClip
	{
		public var swf:MovieClip;

		private var myURL:String;
		private var anzahl:uint;
		
		public var isLoaded:Boolean = false;
		
		//
		public function SWFLoader(locate:String = null)
		{
			
			//anzahl = p_anzahl;
			if (locate != null)
			{
				myURL = locate;
				loadImage();
			}
		}
		private function loadImage():void
		{
			if (myURL != null)
			{
				var mLoader:Loader = new Loader();
				var mRequest:URLRequest = new URLRequest(myURL);
				mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCompleteHandler);
				mLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgressHandler);
				mLoader.load(mRequest);
			}
		}
		
		private function onProgressHandler(mProgress:ProgressEvent)
		{
			var percent:Number = mProgress.bytesLoaded/mProgress.bytesTotal;
			trace(percent);
		}

		private function onCompleteHandler(loadEvent:Event)
		{
			addChild(loadEvent.currentTarget.content);
		}
	}
}