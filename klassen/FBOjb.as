﻿package klassen 
{
	import flash.display.*;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	
	/**
	 * ...
	 * @author 
	 */
	public class FBOjb extends Sprite
	{
		private var _formName:String;
		private var _formID:String;
		private var _message:String;
		private var _picture:String;
		private var _link:String;
		private var _source:String;
		private var _name:String;
		private var _caption:String;
		private var _description:String;
		private var _icon:String;
		private var _attribution:String;
		private var _type:String;
		private var _created_time:String;
		private var _updated_time:String;
		private var _likes:String;
		private var _commernts_count:String;
		
		private var zaehler:uint = 0;
		
		private var obj_formName:TextHTMLCompleteBold;
		private var obj_formMassage:TextHTMLComplete;
		private var obj_picture:BildLoader;
		private var obj_link:TextHTMLComplete;
		private var obj_source:TextHTMLComplete;
		private var obj_name:TextHTMLComplete;
		private var obj_caption:TextHTMLComplete;
		private var obj_description:TextHTMLComplete;
		private var obj_icon:BildLoader;
		private var obj_attribution:TextHTMLComplete;
		private var obj_type:TextHTMLComplete;
		private var obj_created_time:TextHTMLComplete;
		private var obj_updated_time:TextHTMLComplete;
		private var obj_likes:TextHTMLComplete;
		private var obj_commernts_count:TextHTMLComplete;
		
		private var border:Sprite;
		
		private var breite:uint = 250;
		
		private var yPosition:uint = 0;
		
		private var massageTextundLink:String = "";
		private var linkType:String = "";
		
		
		public function FBOjb(p_formName:String, p_formID:String, p_message:String, p_picture:String, p_link:String, p_source:String, p_name:String, p_caption:String, p_description:String, p_icon:String, p_attribution:String, p_type:String, p_created_time:String, p_updated_time:String, p_likes:String, p_commernts_count:String ) 
		{			
			//trace("Zähler: "+zaehler);
			
			_formName = p_formName;
			_formID = p_formID;
			_message = p_message
			_picture = p_picture;
			_link = p_link;
			_source = p_source;
			_name = p_name;
			_caption =p_caption;
			_description = p_description;
			_icon = p_icon;
			_attribution = p_attribution;
			_type = p_type;
			_created_time = p_created_time;
			_updated_time = p_updated_time;
			_likes = p_likes;
			_commernts_count = p_commernts_count;
			
			var spaceOben:Sprite = new Sprite();
			spaceOben.graphics.beginFill (0xffffff);
			spaceOben.graphics.drawRect(0, 0, breite, 8);
			spaceOben.graphics.endFill();
			addChild(spaceOben);
			
			if (_message != "")
			{
				
				var massageLink:LinkSplit = new LinkSplit(_message);
				massageTextundLink = massageLink.GetText();
			}
			
			
			if (_formName != "")
			{
				obj_formName = new TextHTMLCompleteBold("<span class='name'>" + _formName + "</span>", breite);
				obj_formName.y = 11;
				
				obj_formName.addEventListener(HeigthEvent.COMPLETED, donext);
				if (_message == "")
				{
					yPosition = obj_formName.y +5;
				}
			}
		}
		
		private function donext(e:HeigthEvent):void
		{
			switch (e.target) 
			{
				case obj_formName:
					step1(e);
					break;
				case obj_formMassage:
					step1_1(e);
					break;
				case obj_updated_time:
					step2(e);
					break;
				case obj_picture:
					step3(e);
					break;
				case obj_link:
					step4(e);
					break;
				case obj_source:
					step5(e);
					break;
				case obj_name:
					step6(e);
					break;
				case obj_caption:
					step7(e);
					break;
				case obj_description:
					step8(e);
					break;
				case obj_icon:
					step9(e);
					break;
				case obj_attribution:
					step10(e);
					break;
				case obj_type:
					step11(e);
					break;
				case obj_created_time:
					step13(e);
					break;
				case obj_likes:
					step14(e);
					break;
			}
		}
		
		private function step1(e:HeigthEvent):void
		{
			if (_message != "")
			{
				obj_formMassage = new TextHTMLComplete("<span class='name'>" + _formName + "</span>" + "  " + "<span class='massage'>" + massageTextundLink +"</span>", breite);
				obj_formMassage.y = 9;
				addChild(obj_formMassage);
				addChild(obj_formName);
				obj_formMassage.addEventListener(HeigthEvent.COMPLETED, donext);
			}
			else
			{
				obj_formMassage = new TextHTMLComplete("<span class='name'></span>", breite);
				obj_formMassage.addEventListener(HeigthEvent.COMPLETED, donext);
				addChild(obj_formName);
				//step1_1(e);
			}
		}
		
		private function step1_1(e:HeigthEvent):void
		{
			if (_updated_time != "")
			{
				var monatsLabels:Array = new Array("Januar",
                    "Februar",
                    "März",
                    "April",
                    "Mai",
                    "Juni",
                    "Juli",
					"August",
					"September",
					"Oktober",
					"November",
					"Dezember");

				var split1:Array = _updated_time.split("-");
				var split2:Array = split1[2].split("T");
				var split3:Array = split2[1].split(":");
				var tag:String = split2[0];
				var monat_nr:uint = split1[1];
				var monat:String = monatsLabels[monat_nr-1];
				var stunde:String = split3[0];
				var minute:String = split3[1];
				
				

				
				if (_icon != "")
				{
					obj_updated_time= new TextHTMLComplete("<span class='datum'>" + tag + ". " + monat + " um "+ stunde +":" + minute + "</span>", breite - obj_icon.width - 5);
					obj_updated_time.y = yPosition;
					obj_updated_time.x = obj_icon.width + 5;
				}
				else
				{
					obj_updated_time= new TextHTMLComplete("<span class='datum'>" + tag + ". " + monat + " um "+ stunde +":" + minute + "</span>", breite);
					obj_updated_time.y = e.hoehe + yPosition +7;
					//obj_updated_time.x = obj_icon.width + 5;
				}
				yPosition = obj_updated_time.y;
				addChild(obj_updated_time);
				obj_updated_time.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step2(e);
			}
		}
		private function isLoaded(e):void
		{
			
			if (e.target.width > 130) {
					e.target.width = 130;
					e.target.height = 96;
			}
			
		}
		
		private function step2(e:HeigthEvent):void
		{
			if (_type != "link")
			{
				if (_picture != "")
				{
					obj_picture = new BildLoader(_picture);
					obj_picture.y = e.hoehe + yPosition +6;
					//obj_picture.x = 4;
					//border = new Sprite();
					//border.y = e.hoehe + yPosition +8;
					yPosition = obj_picture.y;
					//addChild(obj_picture);
					obj_picture.addEventListener(HeigthEvent.COMPLETED, isLoaded);
					obj_picture.addEventListener(HeigthEvent.COMPLETED, donext);
				}
				else
				{
					step3(e);
				}
			}
			else
			{
				step3(e);
			}
			
		}
		
		private function step3(e:HeigthEvent):void
		{
			if (_type == "link")
			{
				if (_link != "")
				{
					var link:LinkSplit = new LinkSplit(_link);
					linkType = link.GetText();
					obj_link = new TextHTMLComplete("<span class='linkstyle'>" + linkType + "</span>", breite);
					obj_link.y = e.hoehe + yPosition;
					yPosition = obj_link.y;
					addChild(obj_link);
					obj_link.addEventListener(HeigthEvent.COMPLETED, donext);
				}
				else
				{
					step4(e);
				}
			}
			else
			{
				if (_picture != "")
				{
					addChild(obj_picture);
					//border.addChild(obj_picture);
					obj_picture.buttonMode = true;
					
					obj_picture.addEventListener(MouseEvent.MOUSE_OVER, changeBorder);
					obj_picture.addEventListener(MouseEvent.MOUSE_OUT, changeBorder);
					obj_picture.addEventListener(MouseEvent.CLICK, changeBorder);
					
					step4(e);
					
					function changeBorder(e:MouseEvent):void
					{
						switch(e.type)
						{
							case MouseEvent.MOUSE_OVER:
								removeChild(obj_picture);
								addChild(obj_picture);
								break;
							case MouseEvent.MOUSE_OUT:
								removeChild(obj_picture);
								addChild(obj_picture);
								break;
							case MouseEvent.CLICK:
								if (_link != "")
								{
									var request:URLRequest = new URLRequest();
									request.url = _link;
									var windowOrFramename:String = "_blank";
									navigateToURL(request, windowOrFramename);
								}
								break;
								
						}
					}
				
				}
				else
				{
					step4(e);
				}
			}
		}
		
		private function step4(e:HeigthEvent):void
		{
			if (_source != "")
			{
				obj_source = new TextHTMLComplete("<span class='linkstyle'>" + _source + "</span>", breite);
				obj_source.y = e.hoehe + yPosition;
				yPosition = obj_source.y;
				addChild(obj_source);
				obj_source.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step5(e);
			}
		}
		
		private function step5(e:HeigthEvent):void
		{
			if (_name != "")
			{
				obj_name= new TextHTMLComplete("<span class='linkstyle'>" + _name + "</span>", breite);
				obj_name.y = e.hoehe + yPosition;
				yPosition = obj_name.y;
				addChild(obj_name);
				obj_name.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step6(e);
			}
		}
		
		private function step6(e:HeigthEvent):void
		{
			if (_caption != "")
			{
				obj_caption= new TextHTMLComplete("<span class='linkstyle'>" + _caption + "</span>", breite);
				obj_caption.y = e.hoehe + yPosition;
				yPosition = obj_caption.y;
				addChild(obj_caption);
				obj_caption.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step7(e);
			}
		}
		
		private function step7(e:HeigthEvent):void
		{
			if (_description != "")
			{
				obj_description= new TextHTMLComplete("<span class='linkstyle'>" + _description + "</span>", breite);
				obj_description.y = e.hoehe + yPosition;
				yPosition = obj_description.y;
				addChild(obj_description);
				obj_description.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step8(e);
			}
		}
		
		private function step8(e:HeigthEvent):void
		{
			if (_icon != "")
			{
				obj_icon= new BildLoader(_icon);
				obj_icon.y = e.hoehe + yPosition;
				yPosition = obj_icon.y;
				addChild(obj_icon);
				obj_icon.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
				
			}
			else
			{
				step9(e);
			}
		}
		
		private function step9(e:HeigthEvent):void
		{
			if (_attribution != "")
			{
				obj_attribution= new TextHTMLComplete("<span class='linkstyle'>" + _attribution + "</span>", breite);
				obj_attribution.y = e.hoehe + yPosition;
				yPosition = obj_attribution.y;
				addChild(obj_attribution);
				obj_attribution.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step10(e);
			}
		}
		
		
		private function step10(e:HeigthEvent):void
		{
			if (_type != "")
			{
				obj_type= new TextHTMLComplete("<span class='linkstyle'>" + _type + "</span>", breite);
				//obj_type.y = e.hoehe + yPosition;
				//yPosition = obj_type.y;
				//addChild(obj_type);
				obj_type.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step11(e);
			}
		}
		
		private function step11(e:HeigthEvent):void
		{
			step13(e);
			/*if (_created_time != "")
			{
				obj_created_time= new TextHTMLComplete("<span class='datum'>" + _created_time + "</span>", breite);
				obj_created_time.y = e.hoehe + yPosition;
				yPosition = obj_created_time.y;
				addChild(obj_created_time);
				obj_created_time.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step12(e);
			}*/
		}
		
		private function step12(e:HeigthEvent):void
		{
			if (_updated_time != "")
			{
				var monatsLabels:Array = new Array("Januar",
                    "Februar",
                    "März",
                    "April",
                    "Mai",
                    "Juni",
                    "Juli",
					"August",
					"September",
					"Oktober",
					"November",
					"Dezember");

				var split1:Array = _updated_time.split("-");
				var split2:Array = split1[2].split("T");
				var split3:Array = split2[1].split(":");
				var tag:String = split2[0];
				var monat_nr:uint = split2[0];
				var monat:String = monatsLabels[monat_nr-1];
				var stunde:String = split3[0];
				var minute:String = split3[1];
				
				if (_icon != "")
				{
					obj_updated_time= new TextHTMLComplete("<span class='datum'>" + tag + ". " + monat + " um "+ stunde +":" + minute + "</span>", breite - obj_icon.width - 5);
					obj_updated_time.y = yPosition;
					obj_updated_time.x = obj_icon.width + 5;
				}
				else
				{
					obj_updated_time= new TextHTMLComplete("<span class='datum'>" + tag + ". " + monat + " um "+ stunde +":" + minute + "</span>", breite);
					obj_updated_time.y = e.hoehe + yPosition +10;
					//obj_updated_time.x = obj_icon.width + 5;
				}
				yPosition = obj_updated_time.y;
				addChild(obj_updated_time);
				obj_updated_time.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step13(e);
			}
		}
		
		private function step13(e:HeigthEvent):void
		{
			if (_likes != "")
			{
				obj_likes= new TextHTMLComplete("<span class='linkstyle'>" + _likes + "</span>", breite);
				obj_likes.y = e.hoehe + yPosition;
				yPosition = obj_likes.y;
				addChild(obj_likes);
				obj_likes.addEventListener(HeigthEvent.COMPLETED, donext);
				//completeObj();
			}
			else
			{
				step14(e);
			}
		}
		
		private function step14(e:HeigthEvent):void
		{
			if (_commernts_count != "")
			{
				obj_commernts_count= new TextHTMLComplete("<span class='linkstyle'>" + _commernts_count + "</span>", breite);
				obj_commernts_count.y = e.hoehe + yPosition;
				yPosition = obj_commernts_count.y;
				addChild(obj_commernts_count);
				dispatchEvent(new Event(Event.COMPLETE));
				//trace("Obj-Complete!!");
			}
			else
			{
				dispatchEvent(new Event(Event.COMPLETE));
				//trace("Obj-Complete!!");
			}
		}
	}

}