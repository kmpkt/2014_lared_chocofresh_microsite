﻿package klassen
{
	import flash.external.ExternalInterface;

	public class ExternerAufruf
	{
		public function ExternerAufruf(content1:String, content2:String):void
		{
			var greeting:String = String(ExternalInterface.call(content1, content2));
		}
	}

}