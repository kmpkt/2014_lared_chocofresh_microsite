package klassen 
{
	import flash.events.Event;
 
	public class HeigthEvent extends Event   //CountdownEvent-Klasse soll später mein Ereignis sein oder besser eine Instance der CountdownEvent-Klasse 
	{
		//public static const UPDATED : String = "updated";   // deklarieren von zwei Event-Typen
		public static const COMPLETED : String = "completed";
 
		public var hoehe : Number;
 
		public function HeigthEvent ( type : String, bubbles : Boolean = false, cancelable : Boolean = false ) 
		{
			super ( type, bubbles, cancelable );
		}
	}
}