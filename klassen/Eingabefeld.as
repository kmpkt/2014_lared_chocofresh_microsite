package klassen 
{
	import com.adobe.fileformats.vcard.Phone;
	import flash.display.Sprite;
	import fl.controls.Label;
	import fl.controls.TextInput;
	//import fl.managers.FocusManager;

	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author 
	 */
	public class Eingabefeld extends Sprite
	{
		private var breite:uint;
		private var hoehe:uint;
		private var myTextInput:TextInput;
		
		private var regexp:RegExp = /./;
		private	var res:Array;
		
		private var tempString:String;
		private var ex_inputText:String = "";
		private var inputText:String = "";
		public var error_var:Boolean = false;
		private var optional:Boolean;
		
		private var errorborderout:Sprite;
		
		private var event : EingabeEvent;    
		private	var type : String;
			

		
		
		public function Eingabefeld(x:int, y:int, p_breite:uint, p_hoehe:uint, p_inputText:String, p_optional:Boolean = false) 
		{
			breite = p_breite;
			hoehe = p_hoehe;
			ex_inputText = p_inputText
			inputText = p_inputText
			optional = p_optional;
			//fm = new FocusManager(this);

			var format:TextFormat = new TextFormat();
			format.size = 12;
			format.font = "Arial";


			myTextInput = new TextInput();
			//myTextInput.restrict = "0-9A-F";
			//myTextInput.maxChars = 6;
			myTextInput.setSize(breite, hoehe);
			myTextInput.setStyle("textFormat",format);
			myTextInput.move(10, 10);
			myTextInput.appendText(inputText);
			myTextInput.x = x+2;
			myTextInput.y = y+2;
			myTextInput.tabEnabled = true;

			myTextInput.drawFocus(false);
			myTextInput.addEventListener(Event.CHANGE, changeHandler);
			myTextInput.addEventListener(FocusEvent.FOCUS_IN,focusChange);
			myTextInput.addEventListener(FocusEvent.FOCUS_OUT,focusChange);

			
			errorborderout = new Sprite();
			errorborderout.graphics.beginFill(0xCBECFF);
			errorborderout.graphics.drawRect(1,1, myTextInput.width+2, myTextInput.height+2);
			errorborderout.graphics.endFill();
			errorborderout.x = x;
			errorborderout.y = y;
			addChild(errorborderout);
			addChild(myTextInput);
			
			//var myLabel:Label = new Label();
			//myLabel.autoSize = TextFieldAutoSize.LEFT;
			//myLabel.text = myTextInput.length + " of " + myTextInput.maxChars;
			//myLabel.move(10, 30);
			//myLabel.x = myLabel.y = 0;
			//addChild(myLabel);
		}
		
		public function setError():void
		{
			error_var = true;
			removeChild(errorborderout);
			removeChild(myTextInput);
			errorborderout.graphics.beginFill(0xCB0101);
			errorborderout.graphics.drawRect(1,1, myTextInput.width+2, myTextInput.height+2);
			errorborderout.graphics.endFill();
			addChild(errorborderout);
			addChild(myTextInput);
			type = EingabeEvent.ERROR;
			event = new EingabeEvent ( type );   // Porduzieren eines neuen Events mit meinem CountdownEvent-Klasse
			dispatchEvent( event );
		//private var fm:FocusManager;
			
		}
		
		public function clearError():void
		{
			error_var = false;
			removeChild(errorborderout);
			removeChild(myTextInput);
			errorborderout.graphics.beginFill(0xCBECFF);
			errorborderout.graphics.drawRect(1,1, myTextInput.width+2, myTextInput.height+2);
			errorborderout.graphics.endFill();
			addChild(errorborderout);
			addChild(myTextInput);
			type = EingabeEvent.VALIDE;
			event = new EingabeEvent ( type );   // Porduzieren eines neuen Events mit meinem CountdownEvent-Klasse
			dispatchEvent( event );
		}
		
		private function focusChange(e:FocusEvent):void 
		{
			trace(e.type);
			switch(e.type)
			{
				case "focusIn":
					if (ex_inputText == myTextInput.text)
					{
						tempString = e.target.text;
						e.target.text = "";
					}
					break;
				case "focusOut":
					if(e.target.text == "")
					{
						e.target.text = tempString;
						clearError();
					}
					if (e.target.text != ex_inputText)
					{
						if (!Eingabecheck())
						{
							setError();
						}
						else 
						{
							clearError();
						}
					}
					break;
			}
        }

		
		private function selectHandler(event:Event):void
		{
			trace("hallo");
		}
		
		private function changeHandler(event:Event):void
		{
			//inputText = myTextInput.length + " of " + myTextInput.maxChars;
			inputText = myTextInput.text;
			//regexp = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
			trace("TEXT: "+inputText+" : "+regexp.test(inputText));
		}
		
		public function RegexpAusdruck(p_regexp:RegExp):void
		{
			regexp = p_regexp;
		}
		
		/*public function get inputText():String
		{
			return myTextInput.text;
		}*/
		
		public function Eingabecheck():Boolean
		{
			return regexp.test(inputText);
		}
		
		public function IsChange():Boolean
		{
			var change:Boolean;
			if (ex_inputText == myTextInput.text)
			{
				change = false;
			}
			else 
			{
				change = true;
			}
			
			return change;
		}
		
	}

}